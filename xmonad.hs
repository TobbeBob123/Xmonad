-- Read README.org before you dive into my config and change my config.
-- The best way to read README.org is to see my gitlab at https://gitlab.com/TobbeBob123/Xmonad

import XMonad hiding ( (|||) )
import XMonad.Core
import qualified XMonad.StackSet as W

import Graphics.X11.ExtraTypes.XF86

import qualified Data.Map as M
import Data.Monoid
import Data.Maybe
import Data.Semigroup

import System.IO
import System.Exit
import System.Directory

import XMonad.Actions.NoBorders
import XMonad.Actions.SpawnOn
import XMonad.Actions.UpdatePointer
import XMonad.Actions.Navigation2D
import XMonad.Actions.Promote
import XMonad.Actions.WithAll
import XMonad.Actions.CycleWS

import XMonad.Util.EZConfig
import XMonad.Util.NamedActions
import XMonad.Util.WorkspaceCompare
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce
import XMonad.Util.Run

import XMonad.Hooks.ToggleHook
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicProperty

import XMonad.Layout hiding ( (|||) )
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Renamed
import XMonad.Layout.Spiral
import XMonad.Layout.Grid
import XMonad.Layout.SimplestFloat
import XMonad.Layout.TwoPane (TwoPane(..))
import XMonad.Layout.Fullscreen
import XMonad.Layout.SimpleFloat
import XMonad.Layout.ThreeColumns
import XMonad.Layout.MultiColumns
import XMonad.Layout.Gaps
import XMonad.Layout.LimitWindows
import XMonad.Layout.NoBorders
import qualified XMonad.Layout.ToggleLayouts as T
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.ToggleLayouts

myTerminal = "kitty"
myLauncher = "rofi -show drun"

myFocusFollowsMouse  :: Bool
myFocusFollowsMouse  = True

myBorderWidth = 1
myFocusColor = "#ff79c6"
myNormColor   = "#282a36"

myModMask = mod4Mask

soundDir = "~/Sound/"
volumeSound = soundDir ++ "ComputerErrorsoundeffect.mp4"

mySoundPlayer :: String
mySoundPlayer = "ffplay -nodisp -autoexit "

--- WS (1= Emacs, 2= Social, 3= File manager, 4= Webb, 5= Work, 6= Gaming) ---
myWorkspaces = ["Emacs", "Soc", "File", "Web", "Work", "Fun", "7", "8", "9"]

scratchpad = [
  NS "nm" "nm-connection-editor" (className =? "Nm-connection-editor") defaultFloating,
  NS "pavucontrol" "pavucontrol" (className =? "Pavucontrol") defaultFloating,
  NS "geary" "geary" (className =? "Geary") defaultFloating,
  NS "blueman" "blueman-manager" (className =? "Blueman-manager") defaultFloating,
  NS "discord" "discord" (className =? "discord") defaultFloating,
  NS "steam" "steam" (className =? "Steam") defaultFloating,
  NS "spotify" "Spotify" (className =? "Spotify") defaultFloating,
  NS "bitwarden" "bitwarden-desktop" (className =? "Bitwarden") defaultFloating
  ] where role = stringProperty "WM_WINDOW_ROLE"

myHandleEventHook :: Event -> X All
myHandleEventHook = dynamicPropertyChange "WM_NAME" (title =? "Spotify" --> doCenterFloat)

myManageHook = composeAll
               [ className =? "confirm"         --> doCenterFloat
               , className =? "file_progress"   --> doCenterFloat
               , className =? "dialog"          --> doCenterFloat
               , className =? "download"        --> doCenterFloat
               , className =? "error"           --> doCenterFloat
               , className =? "Nm-connection-editor" --> doCenterFloat
               , className =? "Gtk2_prefs" --> doCenterFloat
               , className =? "Steam" --> doCenterFloat
               , className =? "origin.exe" --> doCenterFloat
               , className =? "lunarclient" --> doCenterFloat
               , className =? "Yad" --> doCenterFloat
               , className =? "fim" --> doCenterFloat
               , className =? "Pavucontrol" --> doCenterFloat
               , className =? "CoreImage" --> doCenterFloat
               , className =? "stacer" --> doCenterFloat
               , className =? "Blueman-manager" --> doCenterFloat
               , className =? "Mailspring" --> doCenterFloat
               , className =? "Geary" --> doCenterFloat
               , className =? "discord" --> doCenterFloat
               , className =? "Lutris" --> doCenterFloat
               , className =? "Bitwarden" --> doCenterFloat
               , className =? "Emacs" --> doShift (myWorkspaces !! 0)
               , className =? "Signal" --> doShift (myWorkspaces !! 1)
               , className =? "discord" --> doShift (myWorkspaces !! 1)
               , className =? "Pcmanfm" --> doShift (myWorkspaces !! 2)
               , className =? "LibreWolf" --> doShift (myWorkspaces !! 3)
               , className =? "firefox" --> doShift (myWorkspaces !! 3)
               , className =? "Brave-browser" --> doShift (myWorkspaces !! 3)
               , className =? "Surf" --> doShift (myWorkspaces !! 3)
               , className =? "qutebrowser" --> doShift (myWorkspaces !! 3)
               , className =? "tabbed" --> doShift (myWorkspaces !! 3)
               , className =? "Badwolf" --> doShift (myWorkspaces !! 3)
               , className =? "teams-for-linux" --> doShift (myWorkspaces !! 4)
               , title     =? "LibreOffice" --> doShift (myWorkspaces !! 4)
               , className =? "Soffice" --> doShift (myWorkspaces !! 4)
               , className =? "code-oss" --> doShift (myWorkspaces !! 4)
               , className =? "Steam" --> doShift (myWorkspaces !! 5)
               , className =? "Lutris" --> doShift (myWorkspaces !! 5)
               , className =? "lunarclient" --> doShift (myWorkspaces !! 5)
               , className =? "GeForce NOW" --> doShift (myWorkspaces !! 5)
               , className =? "net-technicpack-launcher-LauncherMain" --> doShift (myWorkspaces !! 5)
               ]

myLayouts = avoidStruts $
            smartBorders $
            gaps [(U,0), (R,0), (L,0), (D,0)] (
            layoutTall
        ||| layoutSpiral
        ||| layoutGrid
        ||| layoutMirror
        ||| layoutFloat
        ||| layoutTreeColumns
        ||| layoutMultiColumns
        ||| layoutTwoPane
        ||| fullscreenFull)
    where
      layoutTall =
                 renamed [Replace "Tall"]
                 $ Tall 1 (3/100) (1/2)
      layoutSpiral =
                 renamed [Replace "Sprial"]
                 $ spiral (6/7)
      layoutGrid =
                 renamed [Replace "Grid"]
                 $ Grid
      layoutMirror =
                 renamed [Replace "Mirror"]
                 $ Mirror (Tall 1 (3/100) (3/5))
      layoutFloat =
                 renamed [Replace "Float"]
                 $ limitWindows 20 simpleFloat
      layoutTreeColumns =
                 renamed [Replace "Treecolumns"]
                 $ ThreeCol 1 (3/100) (1/2)
      layoutMultiColumns =
                 renamed [Replace "Multicolumns"]
                 $ multiCol [1] 1 0.01 (-0.5)
      layoutTwoPane =
                 renamed [Replace "TwoPane"]
                 $ TwoPane (15/100) (6/7)
      fullscreenFull =
                renamed [Replace "Full"]
                $ Full

showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe $ "yad --text-info --fontname=\"Source Code Pro 9\" --fore=#f8f8f2 back=#282a36 --center --geometry=800x500 --title \"XMonad keybindings\""
  --hPutStr h (unlines $ showKm x) -- showKM adds ">>" before subtitles
  hPutStr h (unlines $ showKmSimple x) -- showKmSimple doesn't add ">>" to subtitles
  hClose h
  return ()

myKeys conf = let
     subKeys str ks        = subtitle str : mkNamedKeymap conf ks
     in
-- Tips: <mod>/M = Win key/Super
        subKeys "Program keys"
        -- Start Program Launcher
        [ ("M-d", addName "Start Program Launcher"                                     $ spawn myLauncher)
        -- Start Nett
        , ("M-S-<Tab>", addName "Launch webbrowser"                                    $ spawn "brave")
        -- Start Terminal
        , ("M-e", addName "Launch Terminal"                                            $ spawn myTerminal)
        -- Start Emacs
        , ("M-<Return>", addName "Launch Emacs. An text editor"                        $ spawn "emacsclient -c -a 'emacs'")
        -- Start FilManager
        , ("M-S-f", addName "Launch filebrowser"                                       $ spawn "pcmanfm")
        -- Start Libreoffice
        , ("M-S-t", addName "Launch Libreoffice. An Microsoft Office open source fork" $ spawn "libreoffice")
        ]

        ^++^ subKeys "Border Management"
        -- AV/PÅ Border
        [ ("M-<Esc>", addName "On/Off Borders"                                         $ withFocused toggleBorder)
        ]

        ^++^ subKeys "Xmonad Management"
        -- Lukk Vindu
        [ ("M-S-q", addName "Close Window with focus"                                  $ kill)
        -- Quit xmonad
        , ("M-S-e", addName "Quit Xmonad"                                              $ io (exitWith ExitSuccess))
        -- Restart xmonad
        , ("M-S-r", addName "Restart Xmonad"                                           $ spawn "xmonad --recompile; xmonad --restart")
        -- Show Xmonad config
        , ("M-S-<Return>", addName "Show Xmonad config"                                $ spawn "emacsclient -c -a 'emacs' ~/.xmonad/README.org")
        ]

        ^++^ subKeys "System Management"
        -- Lyd
        [ ("<XF86AudioRaiseVolume>", addName "Increase volume"                         $ sequence_ [spawn (mySoundPlayer ++ volumeSound), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%"])
        , ("<XF86AudioLowerVolume>", addName "Decrease volume"                         $ sequence_ [spawn (mySoundPlayer ++ volumeSound), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%"])
        , ("<XF86AudioMute>", addName "Mute volume"                                    $ sequence_ [spawn (mySoundPlayer ++ volumeSound), spawn "pactl set-sink-mute 0 toggle"])

        -- Lys
        , ("<XF86MonBrightnessUp>", addName "Increase screenlight"                     $ spawn "lux -a 5%")
        , ("<XF86MonBrightnessDown>", addName "Decrease screenlight"                   $ spawn "lux -s 5%")

        -- lås PC
        , ("M-l", addName "Lock the computer"                                          $ spawn "light-locker-command -l")

        -- Ta skjermbilde
        , ("M-p", addName "Take full screen Screenshot"                                $ spawn "~/Script/SkjermBilde.sh")
        , ("M-C-p", addName "Take region Screenshot"                                   $ spawn "~/Script/Flameshot.sh")

        -- System menu (Xmenu)
        , ("M-<Backspace>", addName "Xmenu"                                            $ spawn "~/xmenu/xmenu.sh")
        ]

        ^++^ subKeys "Aliases for the Fish shell"
        -- Vis alias for fish
        [ ("M-S-s", addName "Show alias for Fish"                                      $ spawn "~/.config/fish/alias.sh")
        ]

        ^++^ subKeys "Scratchpad Program"
      --- Scratchpad
        -- Terminal
        -- Web settings (GUI)
        [ ("M-S-n", addName "Launch nm-connection-editor. An Network GUI manager"      $ namedScratchpadAction scratchpad "nm")
        -- Volume control (GUI)
        , ("M-S-l", addName "Launch Pavucontrol. An Gui Volume manager"                $ namedScratchpadAction scratchpad "pavucontrol")
        -- Geary (A Mail client)
        , ("M-m", addName "Launch Mailclient"                                          $ namedScratchpadAction scratchpad "geary")
        -- Blueman manager (Bluetooth device manager)
        , ("M-b", addName "Launch Bluetooth device GUI manager"                        $ namedScratchpadAction scratchpad "blueman")
        -- Discord
        , ("M-f", addName "Launch Discord"                                             $ namedScratchpadAction scratchpad "discord")
        -- Steam
        , ("M-c", addName "Launch Steam"                                               $ namedScratchpadAction scratchpad "steam")
        -- Music
        , ("M-t", addName "Launch music player"                                        $ namedScratchpadAction scratchpad "spotify")
        -- Bitwarden (Password bank)
        , ("M-S-b", addName "Launch Bitwarden"                                         $ namedScratchpadAction scratchpad "bitwarden")
        ]

        ^++^ subKeys "Layout"
--- Layout Hotkeys
        [ ("M-C-1", addName "Switch Layout Tall"                                       $ sendMessage $ JumpToLayout "Tall")
        , ("M-w", addName "Sink all windows to the orginal place"                      $ sinkAll)
        , ("M-C-2", addName "Switch Layout Spiral"                                     $ sendMessage $ JumpToLayout "Sprial")
        , ("M-C-3", addName "Switch Layout Grid"                                       $ sendMessage $ JumpToLayout "Grid")
        , ("M-<Tab>", addName "Circle between layout"                                  $ sendMessage NextLayout)
        , ("M-C-4", addName "Switch Layout Mirror"                                     $ sendMessage $ JumpToLayout "Mirror")
        , ("M-C-5", addName "Switch Layout Float"                                      $ sendMessage $ JumpToLayout "Float")
        , ("M-C-6", addName "Switch Layout Treecolumns"                                $ sendMessage $ JumpToLayout "Treecolumns")
        , ("M-C-7", addName "Switch Layout Multicolumns"                               $ sendMessage $ JumpToLayout "Multicolumns")
        , ("M-C-8", addName "Switch Layout TwoPane"                                    $ sendMessage $ JumpToLayout "TwoPane")
        , ("M-C-9", addName "Switch Layout Full"                                       $ sendMessage $ JumpToLayout "Full")
        , ("M-C-w", addName "Sink window with focus to the orginal place"              $ withFocused $ windows . W.sink)
        ]

        ^++^ subKeys "Window Management"
--- Windows
        [ ("M-a", addName "Change focus to master"                                     $ windows W.focusMaster)
        , ("M-j", addName "Change focus down"                                          $ windows W.focusDown)
        , ("M-k", addName "Change focus up"                                            $ windows W.focusUp)
        , ("M-S-j", addName "Change window place down"                                 $ windows W.swapDown)
        , ("M-S-k", addName "Change window place up"                                   $ windows W.swapUp)
        , ("M-<Space>", addName "Change window place master"                           $ promote)
        , ("M-u", addName "Change window size -"                                       $ sendMessage Shrink)
        , ("M-i", addName "Change window size +"                                       $ sendMessage Expand)
        ]

        ^++^ subKeys "Workspaces Management"
--- Workspaces
        [ ("M-<Right>", addName "Move to next Workspace"                               $ nextWS)
        , ("M-<Left>", addName "Move to prev Workspace"                                $ prevWS)
        , ("M-1", addName "Switch to workspace 1"                                      $ (windows $ W.greedyView $ myWorkspaces !! 0))
        , ("M-2", addName "Switch to workspace 2"                                      $ (windows $ W.greedyView $ myWorkspaces !! 1))
        , ("M-3", addName "Switch to workspace 3"                                      $ (windows $ W.greedyView $ myWorkspaces !! 2))
        , ("M-4", addName "Switch to workspace 4"                                      $ (windows $ W.greedyView $ myWorkspaces !! 3))
        , ("M-5", addName "Switch to workspace 5"                                      $ (windows $ W.greedyView $ myWorkspaces !! 4))
        , ("M-6", addName "Switch to workspace 6"                                      $ (windows $ W.greedyView $ myWorkspaces !! 5))
        , ("M-7", addName "Switch to workspace 7"                                      $ (windows $ W.greedyView $ myWorkspaces !! 6))
        , ("M-8", addName "Switch to workspace 8"                                      $ (windows $ W.greedyView $ myWorkspaces !! 7))
        , ("M-9", addName "Switch to workspace 9"                                      $ (windows $ W.greedyView $ myWorkspaces !! 8))
        , ("M-S-1", addName "Send to workspace 1"                                      $ (windows $ W.shift $ myWorkspaces !! 0))
        , ("M-S-2", addName "Send to workspace 2"                                      $ (windows $ W.shift $ myWorkspaces !! 1))
        , ("M-S-3", addName "Send to workspace 3"                                      $ (windows $ W.shift $ myWorkspaces !! 2))
        , ("M-S-4", addName "Send to workspace 4"                                      $ (windows $ W.shift $ myWorkspaces !! 3))
        , ("M-S-5", addName "Send to workspace 5"                                      $ (windows $ W.shift $ myWorkspaces !! 4))
        , ("M-S-6", addName "Send to workspace 6"                                      $ (windows $ W.shift $ myWorkspaces !! 5))
        , ("M-S-7", addName "Send to workspace 7"                                      $ (windows $ W.shift $ myWorkspaces !! 6))
        , ("M-S-8", addName "Send to workspace 8"                                      $ (windows $ W.shift $ myWorkspaces !! 7))
        , ("M-S-9", addName "Send to workspace 9"                                      $ (windows $ W.shift $ myWorkspaces !! 8))
        ]

        ^++^ subKeys "Screen Management"
--- Skjermer
        [ ("M-S-<Right>", addName "Move window with focus to next Screen"              $ shiftNextScreen)
        , ("M-S-<Left>", addName "Move window with focus to prev Screen"               $ shiftPrevScreen)
        , ("M-<Up>", addName "Move to next screen"                                     $ nextScreen)
        , ("M-<Down>", addName "Move to prev screen"                                   $ prevScreen)
        ]
-- surf to use instead of LibreWolf under "Nett"
--, ("M-S-<Tab>", spawn "surf -SBdI https://startpage.com")

--- Mus ---
myMouseBindings (XConfig {XMonad.modMask = mod}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((mod, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((mod, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((mod, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

myStartupHook :: X ()
myStartupHook = do
                setWMName "X"
                spawnOnce "~/.fehbg"
                spawnOnce "picom --experimental-backends"
                spawnOnce "lxsession"
                spawnOnce "dbus-update-activation-environment --systemd DISPLAY eval $(/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh) export SSH_AUTH_SOCK &"
                spawnOnce "dunst"
                spawnOnce "nm-applet"
                spawnOnce "signal-desktop"
                spawnOnce "trayer --edge top --align right --distance 5 --width 4 --expand true --SetDockType true --SetPartialStrut True --transparent true --alpha 0 --tint 0x282A36 --expand true --height 15 --monitor 1 --padding 1"
                spawnOnce "~/Script/husk_oppdater.sh"
                spawnOnce "blueman-applet"
--                spawnOnce "mailspring"
                spawn "/usr/bin/emacs --daemon"
                spawnOnce "discord"

                -- spawnOnce "xautolock -time 30 -locker -corners ++-- 'systemctl suspend'"

main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
  xmonad $ docks
         $ ewmhFullscreen
         $ withUrgencyHook NoUrgencyHook
         $ defaults {
         logHook = dynamicLogWithPP $ xmobarPP
              {
                 ppTitle = const ""
               , ppTitleSanitize = const ""
               , ppWsSep = " | "
               , ppOutput = hPutStrLn xmproc
               , ppLayout = xmobarColor "#50fa7b" "#282a36"
               , ppCurrent = xmobarColor "#8be9fd" "#282a36"
               , ppVisible = xmobarColor "#ff79c6" "#282a36"
               , ppHiddenNoWindows = xmobarColor "#ff76c6" "#282a36"
               , ppHidden = xmobarColor "#bd93f9" "#282a36"
               , ppUrgent = xmobarColor "#ff5555" "#282a36"
               }
       }

defaults = addDescrKeys' ((mod4Mask, xK_s), showKeybindings) myKeys def {
      -- simple stuff
        focusFollowsMouse  = myFocusFollowsMouse,
        handleEventHook    = myHandleEventHook,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        terminal           = myTerminal,
        -- numlockMask        = myNumlockMask,
        workspaces         = myWorkspaces,

      -- key bindings
        mouseBindings      = myMouseBindings,

        -- hooks, layouts
        layoutHook         = myLayouts,
        normalBorderColor  = myNormColor,
        focusedBorderColor = myFocusColor,
        startupHook        = myStartupHook,
        manageHook         = myManageHook
    } --`additionalKeysP` myKeys
